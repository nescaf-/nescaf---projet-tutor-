﻿using System;
using System.Collections.Generic;
using System.Text;

namespace NesCafe
{
    /// <summary>
    /// The Seed Class Stores Some Data About a Planted Seed, as its Owner
    /// </summary>
    class Seed
    {
        // 0 : No Owner (Default Value)
        // 1 : Player Owned
        // 2 : Enemy Owned
        private readonly byte owner; // Stored as a byte to be lighter

        /// <summary>
        /// Class Constructor
        /// </summary>
        /// 
        /// <param name="owner">Owner's value (see byte owner description) as an int</param>
        public Seed(int owner)
        {
            this.owner = (byte) owner;
        }

        /// <summary>
        /// Owner Getter
        /// </summary>
        /// 
        /// <returns>Owner's Value (as an int for more ease of use)</returns>
        public int GetOwner() =>    (int) owner;

        /// <summary>
        /// Owner's Name Getter as a String (Default value is "." if no owner)
        /// </summary>
        /// 
        /// <returns>Owner's Name as a String</returns>
        public string GetOwnerName =>   (owner == 1) ? "A" : (owner == 2) ? "B" : ".";
    }
}
