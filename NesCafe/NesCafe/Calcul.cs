﻿using System;
using System.Collections.Generic;
using System.Text;

namespace NesCafe
{
    /// <summary>
    /// Only Tells How Many Points Are Awarded for a Player on Designated Coordinates
    /// </summary>
    class Calcul
    {
        // Player Code
        private int player;
        // Position of the Tested Tile. [0]: Absciss [1]: Ordinate
        private int[] position;
        // Score of ally/enemy (to Gamble a Checkmate ("Gamble" because the Values are Estimated))
        private int score_enemy = 0;
        private int score_ally = 0;
        // Gain playing on Designated Coordinates
        private int gain = 0;
        // Position of the Last Seed
        private int[] anchor = null;

        /// <summary>
        /// Updates The Values
        /// </summary>
        /// 
        /// <param name="game_board">Local Copy of the Current Game Board</param>
        /// 
        /// <param name="position">Target position, Where We're Supposed to Simulate a Play</param>
        /// 
        /// <param name="player">What Player (Value) Would be Playing</param>
        /// 
        /// <param name="score">Current Score of Both Players</param>
        /// 
        /// <param name="anchor">The Anchor is The Position of the Last Seed, Restraining Plant() to Work on same Line/ Col Only</param>
        public Calcul(Board game_board, int[] position, int player, int[] score, int[] anchor)
        {
            Update(game_board, position, player, score, anchor);
        }

        #region Getters

        /// <summary>
        /// Returns the Total Estimated Value
        /// </summary>
        /// 
        /// <returns>Move Value as an int</returns>
        public int GetValue() =>    gain;

        #endregion

        #region Manipulation

        /// <summary>
        /// Re-Calculates Gain/Loss
        /// </summary>
        /// 
        /// <param name="game_board">Local Copy of the Current Game Board</param>
        /// 
        /// <param name="position">Target position, Where We're Supposed to Simulate a Play</param>
        /// 
        /// <param name="player">What Player (Value) Would be Playing</param>
        /// 
        /// <param name="score">Current Score of Both Players</param>
        /// 
        /// <param name="anchor">The Anchor is The Position of the Last Seed, Restraining Plant() to Work on same Line/Col Only</param>
        public void Update(Board game_board, int[] position, int player, int[] score, int[] anchor)
        {
            this.player = player;
            this.position = new int[] { position[1], position[0] };
            this.anchor = anchor is null ? null : new int[] { anchor[0], anchor[1] };
            score_ally = score[player - 1];
            score_enemy = score[player % 2];

            Simulate(new Board(game_board));
        }

        /// <summary>
        /// Simulates the Score Gained for Planting a Seed for a Player 
        /// </summary>
        /// 
        /// <param name="sim_board">Copy of the Game Board</param>
        private void Simulate(Board sim_board)
        {
            int temp = sim_board.CalculateScore(player);
            bool plantable = sim_board.IsPlantable(position[1], position[0], anchor);

            if (plantable)
            {
                int old_score = sim_board.CalculateAdvantage(player);

                // Ordinate Absciss
                sim_board.Plant(position[1], position[0], player, anchor); // Plants the Seed
                // Creating a Brand New Area of Only One Tile for Now
                Area new_area = new Area(ref sim_board.GetBoard()[position[1], position[0]]);
                // Absorbs Nearby Areas of the Same Ownership
                sim_board.AreaUpdate(position[0], position[1], ref new_area, player);

                gain = sim_board.CalculateAdvantage(player) - old_score;
            }
            else
                gain = 0;

            // If The We Can Block the enemy (And thus End the Game)
            // Need to be Available to Play and Obstruct
            if (plantable && sim_board.Obstructs(position[0], position[1]))
                // If We can prevent the Enemy from playing (and we have higher score), we basically win
                // We Add a Great Value to the Gain. Otherwise, It Would Be Like Digging Our Own Grave...
                gain += (gain + score_ally) > score_enemy ? 20 : -20; // 20 should be enough
        }

        #endregion
    }
}
