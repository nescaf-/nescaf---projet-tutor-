﻿using System;
using System.Collections.Generic;
using System.Text;

namespace NesCafe
{
    /// <summary>
    /// This Class Basically estimates the value of a move on given coordinates, comparing points earned and points given to the enemy.
    /// Doesn't consider long term: we only estimate one move after the other
    /// </summary>
    class Points
    {
        // Target Tile Position. [0]: Ordinate [1]: Absciss
        private readonly int[] position;
        // Earned Points Playing Here
        private Calcul gain;
        // Score of the Enemy Next Turn if I Play Here
        private Calcul[,] enemy;

        /// <summary>
        /// Constructior. Only Inittializes Tile Position, Other Values Must be Updated via Update()
        /// </summary>
        /// 
        /// <param name="coord_x">Absciss Position of the Target Tile</param>
        /// 
        /// <param name="coord_y">Ordinate Position of the Target Tile</param>
        public Points(Board game_board, int coord_x, int coord_y, int[] score)
        {
            position = new int[] { coord_y, coord_x };

            Init(game_board, score);
        }
        
        /// <summary>
        /// Creates all Calcul Objects Default Values
        /// </summary>
        /// 
        /// <param name="game_board">Copy of the Game's Board</param>
        /// 
        /// <param name="score">Current Score</param>
        private void Init(Board game_board, int[] score)
        {
            enemy = new Calcul[10, 10];

            for (int coord_y = 0; coord_y < 10; coord_y++)
                for (int coord_x = 0; coord_x < 10; coord_x++)
                    enemy[coord_y, coord_x] = new Calcul(game_board, position, 2, score, null);

            gain = new Calcul(game_board, position, 1, score, null);
        }

        #region Getters

        /// <summary>
        /// Gain Getter
        /// </summary>
        /// 
        /// <returns>Direct Gain if we Play Here</returns>
        public int GetGain() => gain.GetValue();

        /// <summary>
        /// Loss Getter
        /// </summary>
        /// 
        /// <returns>Max Enemy Gain Next Turn if we Play Here</returns>
        public int GetLoss()
        {
            int result = 0;

            for (int coord_y = 0; coord_y < 10; coord_y++)
                for (int coord_x = 0; coord_x < 10; coord_x++)
                    if (enemy[coord_y, coord_x].GetValue() > result)
                        result = enemy[coord_y, coord_x].GetValue();

            return result;
        }

        /// <summary>
        /// Total Value of a Move on this tile
        /// </summary>
        /// 
        /// <returns>Gain/Loss Difference</returns>
        public int GetValue() =>    gain.GetValue() - GetLoss();

        #endregion

        #region Manipulation

        /// <summary>
        /// Updates The Values of Every Calcul Object.
        /// </summary>
        /// 
        /// <param name="game_board">Copy of Current Game Board</param>
        /// 
        /// <param name="score">Current Score</param>
        /// 
        /// <param name="anchor">Current Anchor Coordinates</param>
        public void Update(Board game_board, int[] score, int[] anchor)
        {
            gain.Update(game_board, position, 1, score, anchor);

            for (int coord_y = 0; coord_y < 10; coord_y++) // Ordinate
                for (int coord_x = 0; coord_x < 10; coord_x++) // Absciss
                {
                    int[] target = new int[2];
                    target[0] = coord_y; // Absciss
                    target[1] = coord_x; // Ordinate

                    // The Anchor may not Change if the Move is Invalid
                    // if (game_board.GetBoard()[position[0], position[1]].IsAvailable())
                    if (game_board.IsPlantable(position[0], position[1], anchor))
                        enemy[coord_y, coord_x].Update(game_board, target, 2, score, position);
                    else
                        enemy[coord_y, coord_x].Update(game_board, target, 2, score, anchor);
                }
        }

        #endregion
    }
}
