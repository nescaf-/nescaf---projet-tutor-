using System;
using System.Text;

namespace NesCafe
{
    /// <summary>
    /// Main Class
    /// </summary>
    class Game
    {
        // Game Board, Where Everything Related is Stored, like Regions, Seeds, ...
        private static Board board;
        // Server Connection
        private static Connection server;
        // Board Calc Layer Used to Determine the Best Move to do
        private static Points[,] plan;
        // Turn Counter
        private static int turn = 1;
        // Coordinates of the Last Seed Planted. [0]: Line [1]: Col
        private static int[] anchor = null;
        // Game Scoreboard. [0]: Player [1]: Enemy
        private static readonly int[] score = { 0, 0 };

        /// <summary>
        /// Main Program. Gets the server frame and creates a game board out of it.
        /// </summary>
       public static void Main()
        {
            server = new Connection(127, 0, 0, 1, 1213); // localhost
            //server = new Connection(51, 91, 120, 237, 1212);

            Init();

            Play();

            server.CloseConnection();

            Console.ReadLine(); // Pause
        }

        /*public static void Main()
        {
            server = new Connection(127, 0, 0, 1, 1213); // localhost
            //server = new Connection(51, 91, 120, 237, 1212);

            Init();

            for(int test = 1; test < 10; test++)
            {
                UpdatePlan();
                int[] temp = BestMove();

                if (board.IsPlantable(temp[0], temp[1], anchor))
                {// Plants the Seed and Updates The Local Score and Anchor
                    board.Plant(temp[0], temp[1], 1, anchor); //Displaying for Debug
                    anchor = new int[] { temp[0], temp[1] };
                    UpdateScore();
                }

                board.Disp(plan, score);

                Console.WriteLine("\nList of Areas:\n");
                foreach (Area area in board.GetAlliedAreas(1))
                    Console.WriteLine(" Area Size {0}", area.GetSize());
            }
        }*/

        /// <summary>
        /// Initializes Board and Plan's Content
        /// </summary>
        private static void Init()
        {
            board = new Board(server.Get(310));
            plan = new Points[10, 10];

            // Ordinate
            for (int coord_y = 0; coord_y < 10; coord_y++)
                // Absciss
                for (int coord_x = 0; coord_x < 10; coord_x++)
                    plan[coord_y, coord_x] = new Points(board, coord_x, coord_y, score);
        }

        /// <summary>
        /// Plays The Game With a Loop
        /// </summary>
        private static void Play()
        {
            Console.WriteLine("\nAve Caesar, morituri te salutant!\n");
            while (Turn());
        }

        /// <summary>
        /// Plays a Whole Turn And Tells if We Have to Play Another
        /// </summary>
        /// 
        /// <returns>True if Another Turn Must Be Taken</returns>
        private static bool Turn()
        {
            if (turn == 1 || ReadPhase())
            {
                Console.WriteLine("\n<======================= Turn {0} =======================>\n", turn++);

                Think();
                return true;
            }
            return false;
        }

        #region ReadPhase

        /// <summary>
        /// Gets Data From the Server Each Turn
        /// </summary>
        /// 
        /// <returns>If there is a Next Turn, as a Boolean Value</returns>
        private static bool ReadPhase()
        {
            Read(); // TO DO: "Unplant" Management

            return EnemyMove(Read()); // Enemy Move
        }

        /// <summary>
        /// Reads One Message From the Server, prints it on the Console, and Returns it
        /// </summary>
        /// 
        /// <returns>Next Message Sent by the Server</returns>
        private static string Read() => Read(4);

        /// <summary>
        /// Reads One Message From the Server for a Given Length, prints it on the Console, and Returns it
        /// </summary>
        /// 
        /// <param name="length">Number of wanted Characters</param>
        /// 
        /// <returns>Server message</returns>
        private static string Read(int length) => server.Get(length);

        /// <summary>
        /// Reads the Enemy Move and Plants his Seed.
        /// Also Tells if the Game has Ended or if we Play Another Turn.
        /// </summary>
        /// 
        /// <param name="message">Enemy Move from the Server</param>
        /// 
        /// <returns>True if the Game Continues</returns>
        private static bool EnemyMove(string message)
        {
            if (message != "FINI")
                try
                { // Tries Casting the Value from a Sting to a Byte
                    byte temp = Byte.Parse(message.Split(':')[1]);

                    // We Cannot Update the Anchor Just Yet...
                    int temp_x = temp % 10;
                    int temp_y = (temp - temp_x) / 10;

                    if (board.IsPlantable(temp_y, temp_x, anchor))
                    {
                        // ... Because it Would Prevent the Seed to be Planted (same Region)
                        // Plants the Enemy Seed and Updates The Local Enemy Score
                        board.Plant(temp_y, temp_x, 2, anchor);

                        // Now We Can Finally Update Anchor
                        anchor[0] = temp_y;
                        anchor[1] = temp_x;
                    }

                    UpdateScore();

                    // If The Game Ends After the Enemy Move
                    if (Read() == "FINI")
                    {
                        Fini();
                        return false;
                    }
                    // Otherwise, We Continue to Play
                    return true;
                }
                catch (Exception exception)
                {
                    throw exception;
                }

            // Else Ends the Game
            Fini();
            return false;
        }

        /// <summary>
        /// Triggered when Recieved "FINI" From the Server, Reads the Final Score and Ends the Game
        /// </summary>
        private static void Fini()
        {
            Console.WriteLine("\n<============== GAME ENDED AFTER {0} TURNS ==============>\n", --turn);
            // Gets the Final Score from the Server ([S][aa][bb])
            string[] score_as_strings = Read(7).Split(':');

            Console.WriteLine("\nLocal Calculated Score:\nAlly: {0}     Enemy: {1}\n", score[0], score[1]); // Debug

            for (int slot = 1; slot <= 2; slot++)
                try
                {
                    score[slot - 1] = Int32.Parse(score_as_strings[slot]);
                }
                catch(Exception e)
                {
                    throw e;
                }

            Console.WriteLine("\nFinal Score:\nAlly: {0}     Enemy: {1}\n", score[0], score[1]);
        }

        #endregion

        #region ThinkPhase

        /// <summary>
        /// Updates Data, Chooses a Target Tile Accordingly, and Plays on it.
        /// </summary>
        private static void Think()
        {
            UpdatePlan();

            board.Disp(plan, score); // Displays the Game Board

            Play(BestMove());
        }

        /// <summary>
        /// Updates The Plan for the Turn
        /// </summary>
        private static void UpdatePlan()
        {
            for (int coord_y = 0; coord_y < 10; coord_y++)
                for (int coord_x = 0; coord_x < 10; coord_x++)
                    plan[coord_y, coord_x].Update(new Board(board), score, anchor);
        }

        /// <summary>
        /// Gets the Best Move Possible From The List (The Last One if Many Are Equal)
        /// </summary>
        /// 
        /// <returns>Coordinates of the Best Move</returns>
        private static int[] BestMove()
        {
            int gain = 0;
            int[] result = new int[2];

            for (int coord_y = 0; coord_y < 10; coord_y++) // Ordinate
                for (int coord_x = 0; coord_x < 10; coord_x++) // Absciss
                    if (plan[coord_y, coord_x].GetValue() >= gain)
                    {
                        gain = plan[coord_y, coord_x].GetValue();

                        result[0] = coord_y;
                        result[1] = coord_x;
                    }

            return result;
        }

        /// <summary>
        /// Tells the Server Where we Want to Play
        /// </summary>
        /// 
        /// <param name="target">Position of the Target ([0]:y, [1]: x)</param>
        private static void Play(int[] target)
        {
            // Creating a message Readable by the Server
            string message = String.Format("A:" + target[0] + target[1]);

            Console.WriteLine("\nPlaying: {0}\n", message); // "Debug"

            if (board.IsPlantable(target[0], target[1], anchor))
            {// Plants the Seed and Updates The Local Score and Anchor
                Console.WriteLine("Gained {0} points.\n", board.Plant(target[0], target[1], 1, anchor)); //Displaying for Debug
                anchor = new int[] { target[0], target[1] };
            }

            UpdateScore();

            // Sending Info to the Server
            server.Send(message);
        }

        /// <summary>
        /// Updates Both Score Values
        /// </summary>
        private static void UpdateScore()
        {
            score[0] = board.CalculateScore(1);
            score[1] = board.CalculateScore(2);
        }

        #endregion
    }
}
