﻿using System;
using System.Collections.Generic;
using System.Text;

namespace NesCafe
{
    /// <summary>
    /// An Area is a Chain of Seeds Owned by the same Player
    /// </summary>
    class Area
    {
        // List of Tiles in this area
        readonly private List<Tile> tiles = new List<Tile>();

        /// <summary>
        /// Area Contructor
        /// </summary>
        /// 
        /// <param name="tile">Initial Tile</param>
        public Area(ref Tile tile)
        {
            tiles.Add(tile);
        }

        /// <summary>
        /// Constructor by Copy.
        /// </summary>
        /// 
        /// <param name="original">Area to Copy</param>
        public Area(Area original)
        {
            foreach(Tile tile in original.GetTiles())
                tiles.Add(new Tile(tile));
        }

        #region Getters

        /// <summary>
        /// Returns Object
        /// </summary>
        /// 
        /// <returns>This</returns>
        public Area Get() =>    this;

        /// <summary>
        /// Tiles List Getter
        /// </summary>
        /// 
        /// <returns>Tiles List</returns>
        public List<Tile> GetTiles() => tiles;

        /// <summary>
        /// Gives the Number of Seeds in the Area
        /// </summary>
        /// 
        /// <returns>Area Size</returns>
        public int GetSize() => tiles.Count;

        #endregion

        /// <summary>
        /// Tells if  the Area Overlaps the designated Area
        /// </summary>
        /// 
        /// <param name="pos_x">Absciss Position</param>
        /// 
        /// <param name="pos_y">Ordinate Position</param>
        /// 
        /// <returns>Boolean Answer</returns>
        public bool AreaIsHere(int pos_x, int pos_y)
        {
            foreach (Tile tile in tiles)
                if (tile.GetPosX() == pos_x && tile.GetPosY() == pos_y)
                    return true;
            return false;
        }

        /// <summary>
        /// Tests if a Tile is Adjacent to this Area
        /// </summary>
        /// 
        /// <param name="pos_x">Absciss Position of Target Tile</param>
        /// 
        /// <param name="pos_y">Ordinate Position of Target Tile</param>
        /// 
        /// <returns>Boolean Answer</returns>
        public bool AreaIsAdjacentTo(int pos_x, int pos_y)
        {
            foreach (Tile tile in tiles)
                if (tile.IsCloseTo(pos_x, pos_y))
                    return true;
            return false;
        }

        /// <summary>
        /// Transfers All Tiles of another Area into this one to Merge Them
        /// </summary>
        /// 
        /// <param name="input">Area to Absorb</param>
        public void Absorb(ref Area input)
        {
            int slot = 0;
            while (slot < input.GetTiles().Count)
            {
                tiles.Add(input.GetTiles()[slot]); // Adding it to Local List
                input.GetTiles().RemoveAt(slot);
            }
        }
    }
}
