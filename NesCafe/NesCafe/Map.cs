﻿using System;
using System.Collections.Generic;
using System.Text;

namespace NesCafe
{
    /// <summary>
    /// The Map class generates a game board using the server's frame.
    /// We use bytes because we don't need more
    /// We don't create a tile object, because this way,
    /// after the map has finished generating,
    /// we don't have to load multiple objects.
    /// This way, the Map class can be of almost any size.
    /// Map class is Similar to Board Class but are Splitted to be Lighter
    /// </summary>
    class Map
    {
        // The Game Board, Filled with Characters. Used to Create the Board.
        // Each Character Represents a Different Tile.
        private static char[,] map;
        // Last Region Name. Used to Create Actual Region Objects
        private char last_region;


        /// <summary>
        /// Constructor
        /// </summary>
        /// 
        /// <param name="frame_input">Server's Frame</param>
        public Map(String frame_input)
        {
            Process(frame_input);
        }

        /// <summary>
        /// Board Getter
        /// </summary>
        /// 
        /// <returns>Board</returns>
        public char[,] GetMap() =>  map;

        /// <summary>
        /// last_region Getter
        /// </summary>
        /// 
        /// <returns>The Name of the Last Region on the Board</returns>
        public char GetLastRegionName() =>  last_region;

        /// <summary>
        /// Displays the Game Board on the Console
        /// </summary>
        public void Disp()
        {
            for (int Y = 0; Y < map.GetLength(0); Y++)
            {
                for (int X = 0; X < map.GetLength(1); X++)
                    Console.Write("{0} ", map[Y, X]);

                Console.Write("\n");
            }
        }

        #region Char Map Creation

        /// <summary>
        /// Transforms the server's frame ino a game board
        /// </summary>
        /// 
        /// <param name="frame_input">The server's frame, detailing the game's map</param>
        /// 
        /// <returns>A 2D array of chars representing the board</returns>
        private void Process(string frame_input)
        {
            //Creates the Board and converts it to bytes
            byte[,] board_as_bytes = CreateBoard(frame_input);

            //Conversion to Chars
            Mapping(board_as_bytes);

            Disp();
        }

        #region Process

        /// <summary>
        /// Calls board creation and byte conversion functions
        /// </summary>
        /// 
        /// <param name="frame_input">Server's frame</param>
        /// 
        /// <returns>A Board as bytes</returns>
        private byte[,] CreateBoard(string frame_input) =>  BoardToBytes(SplitLines(SplitFrame(frame_input)));

        #region Splitting

        /// <summary>
        /// Splits the frame into lines, as strings
        /// </summary>
        /// 
        /// <param name="frame_input">The server's frame</param>
        /// 
        /// <returns>An array of strings</returns>
        private string[] SplitFrame(string frame_input) =>  frame_input.Split('|');

        /// <summary>
        /// Splits an array of Lines into a 2D array of tiles, as strings
        /// </summary>
        /// 
        /// <param name="lines_as_string"></param>
        /// 
        /// <returns>A game board full of strings</returns>
        private string[,] SplitLines(string[] lines_as_string)
        {
            //The returned 2dim array, filled with strings
            string[,] board_as_strings = new string[lines_as_string.Length - 1, lines_as_string.Length - 1];

            for (int Y = 0; Y < lines_as_string.Length - 1; Y++)
            {
                //Splits the line into strings
                string[] line_as_strings = SplitLine(lines_as_string[Y]);

                for (int X = 0; X < line_as_strings.Length; X++)
                    //Puts the string into the array, then goes to next tile slot
                    board_as_strings[Y, X] = line_as_strings[X];
                //Goes to the first tile of the next line
            }

            return board_as_strings;
        }

        /// <summary>
        /// Splits a line of the frame.
        /// </summary>
        /// 
        /// <param name="line_as_strings">A line from the frame as a string</param>
        /// 
        /// <returns>The same line, but splitted into an array of strings
        /// (using the ':' char.)</returns>
        private string[] SplitLine(string line_as_strings) => line_as_strings.Split(':');

        #endregion

        #region Byte Conversion

        /// <summary>
        /// Converts the board from strings to bytes
        /// </summary>
        /// 
        /// <param name="board_as_strings">The board, filled with strings</param>
        /// 
        /// <returns>The board, now as bytes, usable as numbers</returns>
        private byte[,] BoardToBytes(string[,] board_as_strings)
        {
            //Counters
            int X, Y;
            //Returned object
            byte[,] board_as_bytes = new byte[board_as_strings.GetLength(0), board_as_strings.GetLength(1)];

            for (Y = 0; Y < board_as_strings.GetLength(0); Y++)
                for (X = 0; X < board_as_strings.GetLength(1); X++)
                    board_as_bytes[Y, X] = ToByte(board_as_strings[Y, X]);

            return board_as_bytes;
        }

        /// <summary>
        /// Turns a String into a Byte 
        /// </summary>
        /// 
        /// <param name="string_input">The string to parse</param>
        /// 
        /// <returns>The Byte value of the string</returns>
        private byte ToByte(string string_input)
        {
            byte res;

            try
            {//Tries casting the value from a sting to a byte
                res = Byte.Parse(string_input);
            }
            catch (InvalidCastException e)
            {
                //If Cast could not be done, the map may be corrupted
                //throws Exception
                throw e;
            }

            return res;
        }

        #endregion

        #region Char Definition

        /// <summary>
        /// Initializes then fills the board with corresponding characters
        /// </summary>
        /// 
        /// <param name="board_as_bytes">The untreated game board</param>
        private void Mapping(byte[,] board_as_bytes)
        {
            // Index of the reference tile
            int[] Ref = { 0, 0 };
            // First region's name
            char region = 'a';

            board_as_bytes = WildBoard(board_as_bytes);

            while (Ref[0] != board_as_bytes.GetLength(0)) // GetUnassigned() Condition
            {
                Ref = GetUnassigned(); // Gets an Unassigned tile (named '0')
                if (Ref[0] != board_as_bytes.GetLength(0))
                    region = Zone(Ref, region, board_as_bytes); // Assigns a Name to every tile in its Whole Region
            }

            last_region = (char) (region - 1);
        }

        #region Mapping

        /// <summary>
        /// Removes Forest and Sea tiles values (so we still have frontiers only),
        /// and affects the board if needed
        /// </summary>
        /// 
        /// <param name="board_as_bytes">Game board, as bytes</param>
        /// 
        /// <returns>The board as bytes, but with only the frontier values</returns>
        private byte[,] WildBoard(byte[,] board_as_bytes)
        {
            map = new char[board_as_bytes.GetLength(0), board_as_bytes.GetLength(1)];

            for (int coordy = 0; coordy < board_as_bytes.GetLength(0); coordy++)
                for (int coordx = 0; coordx < board_as_bytes.GetLength(1); coordx++)
                {
                    map[coordy, coordx] = board_as_bytes[coordy, coordx] >= 64
                        ? 'M' : board_as_bytes[coordy, coordx] % 64 >= 32
                        ? 'F' : '0';

                    board_as_bytes[coordy, coordx] %= 32;
                }

            return board_as_bytes;
        }

        /// <summary>
        /// Says what tile to assign next
        /// </summary>
        /// 
        /// <returns>Coordinates of the first unassigned tile</returns>
        private int[] GetUnassigned()
        {
            int X = 0, Y = 0;

            //Accepts any board size
            while (Y != map.GetLength(0) && map[Y, X] != '0')
                if (X < map.GetLength(1) - 1)
                    X++;
                else
                {
                    X = 0;
                    Y++;
                }

            //Returns unassigned tile's slot
            //If there's none, returns board size
            return new int[] { Y, X };
        }

        /// <summary>
        /// Defines a tile's name, based on it's value
        /// </summary>
        /// 
        /// <param name="Ref">Coordiantes of the reference tile
        /// Reference tile is used to know when to finish the loop</param>
        /// 
        /// <param name="region_name">name of the current region</param>
        /// 
        /// <param name="board_as_bytes">Game board, as bytes</param>
        /// 
        /// <returns>Name of the next region</returns>
        private char Zone(int[] Ref, char region_name, byte[,] board_as_bytes)
        {
            Scout(Ref, region_name++, board_as_bytes);
            // We Define a region and changes the region name (for the next one)

            return region_name;
        }

        #region Zoning

        /// <summary>
        /// Defines an area using its frontiers,
        /// as if we would "scout" the region,
        /// following the frontiers
        /// </summary>
        /// 
        /// <param name="Ref">Reference Tile</param>
        /// 
        /// <param name="region_name">the current region's name (a, b, c, ...)</param>
        /// 
        /// <param name="board_as_bytes">Game board, as bytes</param>
        private void Scout(int[] Ref, char region_name, byte[,] board_as_bytes)
        {
            // Copies the Ref values: Ref will be the reference, and tile the tile we're testing
            int[] tile = new int[] { Ref[0], Ref[1] }; // Currently tested tile
            int move = 0; // First direction tested

            do
            {
                map[tile[0], tile[1]] = region_name; //We name the tile according to the region
                move = MovementTest(move, board_as_bytes[tile[0], tile[1]]); //We tests how we can move
                tile = Move(tile, move--); //We move in that direction

                // Next tile will try moving the direction before this one first
                // If we go North (0), we'll try East (3) first for the next tile 
                move = move == -1 ? 3 : move;
            } while (tile[0] != Ref[0] || tile[1] != Ref[1]);
            //We stop after returning to the reference tile
        }

        /// <summary>
        /// Tests Where to move Next.
        /// We try to move in the "first" direction first,
        /// if it's 1, for N, next we test 2, for W,
        /// then 3 (S), and 4 (E).
        /// Stops after 1 trun (or if we find a valid direction)
        /// </summary>
        /// 
        /// <param name="first">The first direction we try to move in</param>
        /// 
        /// <param name="tilevalue">Tested tile's value</param>
        /// 
        /// <returns>The first direction we can move in, starting from "first"</returns>
        private int MovementTest(int first, byte tilevalue)
        {
            int direction = first % 4; // %4 is just in case it's not already from 0 to 3.
            int tested = 0; // Counts how many directions have been tested (stops loop if = 4)

            do
            {
                // Tests the frontier on this direction and Gets next direction's value
                direction = Frontiers(tilevalue)[direction] ? (direction + 1) % 4 : direction;
            } while (Frontiers(tilevalue)[direction] && ++tested <= 4);

            return direction;
        }

        /// <summary>
        /// Says what are a tile's frontiers
        /// </summary>
        /// 
        /// <param name="tile">A tile of the game board, as byte</param>
        /// 
        /// <returns>An array of booleans representing a tile's frontiers</returns>
        private bool[] Frontiers(byte tile)
        {
            bool[] frontiers = new bool[4];

            // Testing frontier E, then S, W, and N
            for (int direction = 3; direction >= 0; direction--)
            {
                // Tests power's value
                if (tile >= (Math.Pow(2, direction)))
                {
                    // If there's a Frontier We Declare it
                    frontiers[direction] = true; 
                    // We remove its value so we can test the next one
                    tile -= (byte)(Math.Pow(2, direction));
                }
                else
                    frontiers[direction] = false; //No Frontier
            }

            return frontiers;
        }

        /// <summary>
        /// Moves from a tile to another, next to it
        /// </summary>
        /// 
        /// <param name="tile">The tile we're on</param>
        /// 
        /// <param name="direction">The direction we're moving in</param>
        /// 
        /// <returns>The new tile's coords, after moving</returns>
        private int[] Move(int[] tile, int direct)
        {
            _ = direct == 0
                ? tile[0]-- : direct == 1
                ? tile[1]-- : direct == 2
                ? tile[0]++ : direct == 3
                ? tile[1]++ : 0;

            return tile;
        }

        #endregion

        #endregion

        #endregion

        #endregion

        #endregion
    }
}