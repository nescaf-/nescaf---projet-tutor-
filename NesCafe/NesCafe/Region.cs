﻿using System;
using System.Collections.Generic;

namespace NesCafe
{
    /// <summary>
    /// The Region Class is Affected to a group of Tiles that are Connected, and Provides a ScoreBonus if all Tiles are Controlled by a Player.
    /// </summary>
    class Region
    {
        // Region's Name
        private readonly char name = '0';
        // List of every Tile Located in this Region
        private readonly List<Tile> tile_list = new List<Tile>();

        /// <summary>
        /// Constructor. Only Sets the Region Name, Tiles must be added One by One after that
        /// </summary>
        /// 
        /// <param name="name">The name of the Region on the Board</param>
        public Region(char name)
        {
            this.name = name;
        }

        /// <summary>
        /// Constructor. Copy of the Argument
        /// </summary>
        /// 
        /// <param name="original">Original Region to Copy</param>
        public Region(Region original) : this(original.GetName())
        {
            foreach (Tile tile in original.GetList())
                tile_list.Add(new Tile(tile));
        }

        #region Getters

        /// <summary>
        /// Region Name Getter
        /// </summary>
        /// 
        /// <returns>The Region's Name</returns>
        public char GetName() =>    name;

        /// <summary>
        /// Gets the List of Tiles Within the Region
        /// </summary>
        /// 
        /// <returns>tile_list</returns>
        public List<Tile> GetList() =>  tile_list;

        /// <summary>
        /// Tells the Number of Tiles in the Region
        /// </summary>
        /// 
        /// <returns>tile_list Size</returns>
        public int GetSize() => tile_list.Count;

        /// <summary>
        /// Tells if the Region is Owned by an AI, and which one if so
        /// </summary>
        /// 
        /// <returns>Owner's Value (see Seed Class Documentation) as int</returns>
        public int GetOwner()
        {
            int[] counter = { 0, 0 };

            // Each Tile is Tested and Added to the total counter
            foreach (Tile tile in tile_list)
                _ = tile.GetOwner() == 1 ? ++counter[0] : tile.GetOwner() == 2 ? ++counter[1] : 0;

            // Compares and Returns Owner's Code.
            return counter[0] == counter[1] ? 0 : counter[0] > counter[1] ? 1 : 2;
        }

        #endregion

        #region Manipulation

        /// <summary>
        /// Adds a reference of a tile to the List
        /// </summary>
        /// 
        /// <param name="tile">A Tile Being Part of the Region</param>
        public void LinkTile(ref Tile tile)
        {
            tile_list.Add(tile);
        }

        #endregion
    }
}