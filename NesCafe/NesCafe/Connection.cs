﻿using System;
using System.Text;
using System.Net; // network tools
using System.Net.Sockets; // sockets


namespace NesCafe
{
    class Connection
    {
        private static readonly Socket server = new Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.Tcp);

        private readonly IPAddress host;
        private readonly int port = 25565; // Ususal Default Value
        private bool is_connected = false;

        #region Constructor

        /// <summary>
        /// Constructor. Builds the IP with Byte Elements.
        /// </summary>
        /// 
        /// <param name="first">First Byte of the Server IP</param>
        /// 
        /// <param name="second">Second Byte of the Server IP</param>
        /// 
        /// <param name="third">Third Byte of the Server IP</param>
        /// 
        /// <param name="fourth">Fourth Byte of the Server IP</param>
        /// 
        /// <param name="port">Server Port</param>
        public Connection(int first, int second, int third, int fourth, int port)
        {
            byte[] IP = Compile(ToByte(first), ToByte(second), ToByte(third), ToByte(fourth));
            this.port = port;
            
            host = new IPAddress(IP);
            Connect();
        }

        #endregion

        #region Connection

        /// <summary>
        /// Connects to the Server with Previously Defined Parameters
        /// </summary>
        public void Connect()
        {
            if (is_connected)
                Console.WriteLine("Already Connected.");
            else
            {
                Console.WriteLine("Connecting...");
                server.Connect(host, port);
                is_connected = true;
                Console.WriteLine("Connected.");
            }
        }

        /// <summary>
        /// Closes Connection with the Server
        /// </summary>
        public void CloseConnection()
        {
            if (is_connected)
            {
                server.Close();
                Console.WriteLine("Connection Ended.");
            }
            else
                Console.WriteLine("No Connection to End.");
            is_connected = false;
        }

        #endregion

        #region Getters

        /// <summary>
        /// Server Getter
        /// </summary>
        /// 
        /// <returns>Server</returns>
        public Socket GetSocket() =>    server;

        /// <summary>
        /// Tells if We're Connected to the Server
        /// </summary>
        /// 
        /// <returns>Boolean value of Connection</returns>
        public bool IsConnected() =>    is_connected;

        #endregion

        #region Communication

        /// <summary>
        /// Translates and Sends a Given Message to the Server
        /// </summary>
        /// 
        /// <param name="message">Data to Send</param>
        public void Send(string message)
        {
            if (is_connected)
                try
                {// Turns the Message into an array of bytes and Sends it
                    server.Send(Translate(message.ToCharArray()));
                    Console.WriteLine("Message Sent.");
                }
                catch (Exception exception)
                {
                    throw exception;
                }
            else
                Console.WriteLine("Couldn't Send the Message: Not Connected.");
        }

        /// <summary>
        /// Listens to the Server
        /// </summary>
        /// 
        /// <returns>Server Message</returns>
        public string Get(int length)
        {
            if (is_connected)
            {
                byte[] received = new byte[length];
                int size = server.Receive(received); // Receives the Size of the Server's Byte Array
                string frame = Encoding.ASCII.GetString(received);
                frame = frame.Trim(); // Clean
                received = Encoding.ASCII.GetBytes(frame);
                Console.WriteLine("Received {0} bytes on {1}.", size, received.Length); // Debug

                /* (Debug)
                 * foreach (Byte data in received)
                 *  Console.WriteLine("[]={0}", data); */

                if (size > 0)
                    Console.WriteLine("Received: \"" + frame + "\".\n"); // Actual Received Array

                return frame;
            }

            Console.WriteLine("No Connection to the Server.");

            return "";
        }

        #endregion

        #region Dependencies

        /// <summary>
        /// Translates an Array of Char Into an Array of Bytes
        /// </summary>
        /// 
        /// <param name="input">Message to Translate</param>
        /// 
        /// <returns>Message as an Array of Bytes</returns>
        private byte[] Translate(char[] input)
        {
            byte[] output = new byte[input.Length];

            int index = 0;
            foreach (char character in input)
                output[index++] = (byte) character;

            return output;
        }

        /// <summary>
        /// Casts an Int into a Byte
        /// </summary>
        /// 
        /// <param name="value">Value as Int</param>
        /// 
        /// <returns>Value as Byte</returns>
        private byte ToByte(int value)
        {
            try
            {
                return (byte) value;
            }
            catch(Exception exception)
            {
                throw exception;
            }
        }

        /// <summary>
        /// Turns 4 bytes into an Array of Bytes
        /// </summary>
        /// 
        /// <param name="part1">Byte for Index #0</param>
        /// 
        /// <param name="part2">Byte for Index #1</param>
        /// 
        /// <param name="part3">Byte for Index #2</param>
        /// 
        /// <param name="part4">Byte for Index #3</param>
        /// 
        /// <returns>Array of Bytes</returns>
        private byte[] Compile(byte part1, byte part2, byte part3, byte part4) =>   new byte[] { part1, part2, part3, part4 };

        #endregion
    }
}
