﻿using System;
using System.Collections.Generic;
using System.Text;

namespace NesCafe
{
    /// <summary>
    /// Tile Class represents One Particular Tile on the Map, at Given Coordinates
    /// </summary>
    class Tile
    {
        // Tile Position on the Board [0]: x (Absciss) [1]: y (Ordinate)
        private readonly int[] position = new int[] { 0, 0 };
        // True if a seed can be planted on this tile
        private bool available;
        // Name of Parent Region
        private char region_name = '0'; // Default Value
        // Seed planted on this tile
        // No getter since we use this class to access its data
        private Seed seed = null; // Null Until a Seed is Placed

        /// <summary>
        /// Initializes the Tile's Values
        /// </summary>
        /// 
        /// <param name="pos_x">Absciss Position on the Board</param>
        /// 
        /// <param name="pos_y">Ordinate Position on the Board</param>
        /// 
        /// <param name="value">Frame value translated as a Character</param>
        public Tile(int pos_x, int pos_y, char value)
        {
            position[0] = pos_x;
            position[1] = pos_y;
            SetAvailability(value);
        }

        /// <summary>
        /// Contructor. Creates a Copy of the Givent Tile
        /// </summary>
        /// 
        /// <param name="original">Original Tile to Duplicate</param>
        public Tile(Tile original) : this(original.GetPosY(), original.GetPosX(), original.GetRegionName())
        {
            if (original.IsGrowing())
                Plant(original.GetOwner());
        }

        #region Init

        /// <summary>
        /// Checks if we Can Plant a Seed on this Tile or not and Sets the Correct Value
        /// </summary>
        /// 
        /// <param name="value">Frame Character (Constructor Argument)</param>
        private void SetAvailability(char value)
        {
            // If it's a Sea or Forest Tile, it's Unfertile and we Lock it
            available = value != 'M' && value != 'F';

            region_name = value;
        }

        #endregion

        #region Getters

        /// <summary>
        /// X Position Getter
        /// </summary>
        /// 
        /// <returns>Absciss Position</returns>
        public int GetPosX() => position[0];

        /// <summary>
        /// Y Position Getter
        /// </summary>
        /// 
        /// <returns>Ordinate Position</returns>
        public int GetPosY() => position[1];

        /// <summary>
        /// returns the Region's Name
        /// </summary>
        /// 
        /// <returns>Region Name Character</returns>
        public char GetRegionName() =>  region_name;

        /// <summary>
        /// Tells if a Seed can be Placed on this Tile
        /// </summary>
        /// 
        /// <returns>Tile's Availability</returns>
        public bool IsAvailable() =>    available;

        /// <summary>
        /// Tells if a Seed is Growing on this Tile
        /// </summary>
        /// 
        /// <returns>True if a Seed is planted</returns>
        public bool IsGrowing() =>  seed != null;

        /// <summary>
        /// Gets the Seed's Owner (Accessed from Tile Class for Lighter Code)
        /// </summary>
        /// 
        /// <returns>Owner Value (see Seed Class)</returns>
        public int GetOwner() =>    IsGrowing() ? seed.GetOwner() : 0;

        /// <summary>
        /// Gets the Seed's Owner
        /// </summary>
        /// 
        /// <returns>Owner's Name ("." if no Owner)</returns>
        public string GetOwnerName() => IsGrowing() ? seed.GetOwnerName : ".";

        #endregion

        #region Manipulation

        /// <summary>
        /// Plants a Seed on the Tile (Availability Check is done Beforehand to Plant())
        /// </summary>
        /// 
        /// <param name="owner">Owner's Value (see Seed Class)</param>
        /// 
        /// <returns>This Tile Instance</returns>
        public void Plant(int owner)
        {
            if (available)
            {
                seed = new Seed(owner);
                available = false;
            }
        }

        /// <summary>
        /// Tests if a given Tile is Adjacent to given Coordinates
        /// </summary>
        /// 
        /// <param name="pos_x">Absciss Position</param>
        /// 
        /// <param name="pos_y">Ordinate Position</param>
        /// 
        /// <returns>If Tile is Adjascent to Coordinates</returns>
        public bool IsCloseTo(int pos_x, int pos_y) =>  TestPosition(pos_x + 1, pos_y)
                                                        || TestPosition(pos_x - 1, pos_y)
                                                        || TestPosition(pos_x, pos_y + 1)
                                                        || TestPosition(pos_x, pos_y - 1);

        /// <summary>
        /// Tells if Given Position is Equal to This Tile's Location
        /// </summary>
        /// 
        /// <param name="pos_x">Absciss</param>
        /// 
        /// <param name="pos_y">Ordinate</param>
        /// 
        /// <returns>Boolean Answer</returns>
        public bool TestPosition(int pos_x, int pos_y) =>   position[0] == pos_x && position[1] == pos_y;

        #endregion
    }
}
