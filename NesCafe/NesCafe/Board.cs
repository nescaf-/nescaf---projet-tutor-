﻿using System;
using System.Collections.Generic;
using System.Net.NetworkInformation;
using System.Reflection;
using System.Text;

namespace NesCafe
{
    /// <summary>
    /// Actual Game Board Class
    /// Map Class is a Pre-Generated char Array Ready to be Used by Board
    /// </summary>
    class Board
    {
        // Actual Board, Filled with Tiles
        private Tile[,] board;
        // List of All Regions (Defined by Frontiers)
        private readonly List<Region> regions = new List<Region>();
        // List of All Controlled Areas (Chain of Seeds)
        private readonly List<Area> player_areas = new List<Area>();
        // List of All Areas Controlled by the Enemy
        private readonly List<Area> rival_areas = new List<Area>();

        /// <summary>
        /// Initializes The Game's Board
        /// </summary>
        /// 
        /// <param name="frame_input">Server's Map Frame</param>
        public Board(string frame_input)
        {
            Map map = new Map(frame_input);
            Init(map);
        }

        /// <summary>
        /// Constructor. Creates a Copy of the Given Board
        /// </summary>
        /// 
        /// <param name="original">Original Board to Copy Data From</param>
        public Board(Board original)
        {
            // Copy of Board
            board = new Tile[10, 10];
            for (int pos_y = 0; pos_y < 10; pos_y++)
                for (int pos_x = 0; pos_x < 10; pos_x++)
                    board[pos_y, pos_x] = new Tile(original.GetBoard()[pos_y, pos_x]);

            // Copy of Regions
            foreach (Region region in original.GetRegions())
                regions.Add(new Region(region));
            FillRegions();

            // Copy of Server Areas
            foreach (Area area in original.GetEnemyAreas(1))
                rival_areas.Add(new Area(area));

            // Copy of Player Areas
            foreach (Area area in original.GetAlliedAreas(1))
                player_areas.Add(new Area(area));
        }

        #region Init

        /// <summary>
        /// Creates the Tile Board and Regions List
        /// </summary>
        /// 
        /// <param name="map">Pre Generated Map</param>
        private void Init(Map map)
        {
            board = new Tile[10, 10];
            SetTiles(map.GetMap());
            SetRegions(map.GetLastRegionName());
            FillRegions();
            CleanRegionList();
        }

        /// <summary>
        /// Creates Tile Board using Characters Map
        /// </summary>
        /// 
        /// <param name="map">Game Board as Characters</param>
        private void SetTiles(char[,] map)
        {
            for (int pos_y = 0; pos_y < 10; pos_y++)
                for (int pos_x = 0; pos_x < 10; pos_x++)
                    // Creates a Tile Corresponding to the Map's Character
                    board[pos_y, pos_x] = new Tile(pos_x, pos_y, map[pos_y, pos_x]);
        }

        /// <summary>
        /// Creates Every Region the Board Needs
        /// </summary>
        /// 
        /// <param name="last_region">Name of the Last Region</param>
        private void SetRegions(char last_region)
        {
            char region_name = 'a'; // First Region Name

            do
            {
                // Creates a New Region and Adds it to the List
                regions.Add(new Region(region_name++));
            } while (region_name <= last_region); // For Every Region on the Board
        }

        /// <summary>
        /// Connects Every Tile to its Region (Except for Forest and Sea Tiles, of Course)
        /// </summary>
        private void FillRegions()
        {
            foreach (Region region in regions)
                for (int pos_y = 0; pos_y < 10; pos_y++)
                    for (int pos_x = 0; pos_x < 10; pos_x++)
                        // If a Tile has the Correct Region Name
                        if (board[pos_y, pos_x].GetRegionName() == region.GetName())
                            // The Tile Goes to the Ragion's Tile List
                            region.LinkTile(ref board[pos_y, pos_x]);
        }

        /// <summary>
        /// Removes Empty Regions from the List
        /// </summary>
        public void CleanRegionList()
        {
            int slot = 0;

            while (slot < regions.Count)
                // If a Region's Empty
                if (regions[slot].GetList().Count == 0)
                    regions.RemoveAt(slot); // Removes it
                else
                    slot++;
        }

        #endregion

        #region Getters

        /// <summary>
        /// Board Getter
        /// </summary>
        /// 
        /// <returns>Game's Board</returns>
        public Tile[,] GetBoard() =>    board;

        /// <summary>
        /// Regions List Getter
        /// </summary>
        /// 
        /// <returns>List of All Regions</returns>
        public List<Region> GetRegions() => regions;

        /// <summary>
        /// Gets Name of the Region Covering the Given Coordinates
        /// </summary>
        /// 
        /// <param name="pos_x">Absciss of the Targeted Position</param>
        /// 
        /// <param name="pos_y">Ordinate of the Targeted Position</param>
        /// 
        /// <returns>Name of Region</returns>
        public char GetRegionName(int pos_x, int pos_y) => board[pos_y, pos_x].GetRegionName();

        /// <summary>
        /// Returns a List of Areas owned by the designated player
        /// </summary>
        /// 
        /// <param name="owner">Code of the Player Possessing the Areas to Fetch</param>
        /// 
        /// <returns>A List of Areas</returns>
        public List<Area> GetAlliedAreas(int player) => player == 1 ? player_areas : rival_areas;

        /// <summary>
        /// Gets Size of the Largest Area of the Designated Player
        /// </summary>
        /// 
        /// <param name="player">Target</param>
        /// 
        /// <returns>Size of the Largest Area</returns>
        public int LargestAlliedArea(int player) => player == 1 ? Largest(player_areas) : Largest(rival_areas);

        /// <summary>
        /// Returns a List of Areas owned by the designated player
        /// </summary>
        /// 
        /// <param name="player">Allied Player. We Fetch the Enemy's List</param>
        /// 
        /// <returns>A List of Areas</returns>
        public List<Area> GetEnemyAreas(int player) => player == 2 ? rival_areas : player_areas;

        /// <summary>
        /// Gets Size of the Largest Area of the Enemy of the Designated Player
        /// </summary>
        /// 
        /// <param name="player">Target</param>
        /// 
        /// <returns>Size of the Largest Area</returns>
        public int LargestEnemyArea(int player) => player == 2 ? Largest(rival_areas) : Largest(player_areas);

        /// <summary>
        /// Tells if Placing a Seed in given Coordinates Blocks the Enemy (and thus Ends the Game)
        /// </summary>
        /// 
        /// <param name="pos_x">Absciss Position on board</param>
        /// 
        /// <param name="pos_y">Ordinate Position on Board</param>
        /// 
        /// <returns>Boolean Answer</returns>
        public bool Obstructs(int pos_x, int pos_y)
        {
            // We increment a placeholder coordinate
            // For Every Tile on the Same Absciss OR Ordinate...
            for (int coord = 0; coord < board.GetLength(1); coord++)
            {
                // ... If the slot is Available
                if (board[pos_y, coord].IsAvailable())
                    // If it's in Another Region...
                    if (GetRegionName(pos_x, pos_y) != GetRegionName(coord, pos_y) )
                        // Then It doesn't Obstruct the Enemy
                        return false;

                // Same with the Other Coordinate
                if (board[coord, pos_y].IsAvailable())
                    if (GetRegionName(pos_x, pos_y) != GetRegionName(pos_x, coord))
                        return false;
            }
            //Else, it does
            return true;
        }

        /// <summary>
        /// Tells if a Seed Can be Planted on Designated Coordiantes According to Anchor
        /// </summary>
        /// 
        /// <param name="pos_y">Ordinate Pos</param>
        /// 
        /// <param name="pos_x">Absciss Pos</param>
        /// 
        /// <param name="anchor">Anchor to be tested</param>
        /// 
        /// <returns>True if Plantable</returns>
        public bool IsPlantable(int pos_y, int pos_x, int[] anchor)
        {
            return board[pos_y, pos_x].IsAvailable() && (anchor is null || (pos_x == anchor[1] || pos_y == anchor[0])
                && board[anchor[0], anchor[1]].GetRegionName() != board[pos_y, pos_x].GetRegionName());
        }

        #endregion

        #region Display

        /// <summary>
        /// Displays Every Layers of the Game Board for the Turn
        /// </summary>
        /// 
        /// <param name="plan">Game Plan to Display</param>
        /// 
        /// <param name="score">Current Score</param>
        public void Disp(Points[,] plan, int[] score)
        {
            DispScore(score);
            DispRegions();
            DispSeeds();
            DispPlan(plan);
        }

        /// <summary>
        /// Displays Both Players Score
        /// </summary>
        /// 
        /// <param name="score">Current Game Score</param>
        public void DispScore(int[] score)
        {
            Console.WriteLine("Score A: {0}.     Score B: {1}.\n", score[0], score[1]);
        }

        /// <summary>
        /// Displays the Regions Map on the Console
        /// </summary>
        public void DispRegions()
        {
            Console.WriteLine("\n\nRegions :\n"); // Title

            string line;
            for(int pos_y = 0; pos_y < board.GetLength(0); pos_y++)
            {
                line = " ";
                for (int pos_x = 0; pos_x < board.GetLength(1); pos_x++)
                {
                    line += " ";
                    line += board[pos_y, pos_x].GetRegionName();
                }
                Console.WriteLine(line);
            }
        }

        /// <summary>
        /// Displays the Players Positions Calc Layer
        /// </summary>
        public void DispSeeds()
        {
            Console.WriteLine("\n\nSeeds :\n"); // Title

            string line;
            for (int pos_y = 0; pos_y < board.GetLength(0); pos_y++)
            {
                line = " ";
                for (int pos_x = 0; pos_x < board.GetLength(1); pos_x++)
                {
                    line += " ";
                    line += board[pos_y, pos_x].GetOwnerName();
                }
                Console.WriteLine(line);
            }
        }

        /// <summary>
        /// Displays the Estimated points gained Calc Layer
        /// </summary>
        /// 
        /// <param name="plan">Game Plan for the Turn</param>
        public void DispPlan(Points[,] plan)
        {
            Console.WriteLine("\n\nPlan :\n"); // Title

            string line;
            for (int pos_y = 0; pos_y < board.GetLength(0); pos_y++)
            {
                line = " ";
                for (int pos_x = 0; pos_x < board.GetLength(1); pos_x++)
                    line = String.Format(line + " | " + plan[pos_y, pos_x].GetValue() + "(+" + plan[pos_y, pos_x].GetGain() + "/-" + plan[pos_y, pos_x].GetLoss() + ")");
                    // We had to Pass Plan var Because it had to be Initialized in Game Class
                Console.WriteLine(line);
            }
        }

        #endregion

        #region Manipulation

        /// <summary>
        /// Plants a Seed and Updates Areas
        /// </summary>
        /// 
        /// <param name="pos_y">Ordinate Position of the Seed</param>
        /// 
        /// <param name="pos_x">Absciss Position of the Seed</param>
        /// 
        /// <param name="owner">Current Player's Value</param>
        /// 
        /// <param name="anchor">The Anchor is The Position of the Last Seed, Restraining Plant() to Work on same Line/ Col Only</param>
        /// 
        /// <returns>Points Gained</returns>
        public int Plant(int pos_y, int pos_x, int owner, int[] anchor)
        {
                       // Ordinate Absciss
            if (IsPlantable(pos_y, pos_x, anchor))
            {
                int old_score = CalculateAdvantage(owner);

                 // Ordinate Absciss
                board[pos_y, pos_x].Plant(owner); // Plants the Seed
                // Creating a Brand New Area of Only One Tile for Now
                Area new_area = new Area(ref board[pos_y, pos_x]);
                // Absorbs Nearby Areas of the Same Ownership
                AreaUpdate(pos_x, pos_y, ref new_area, owner);

                return CalculateAdvantage(owner) - old_score;
            }

            return 0;
        }

        #region Score Calculation

        /// <summary>
        /// Calculates and Returns Current Score of Given player
        /// </summary>
        /// 
        /// <param name="owner">Player's Number</param>
        /// 
        /// <returns>Calculated Score</returns>
        public int CalculateScore(int owner)
        {
            int score = GetRegionScore(owner);
            score += owner == 1
                ? Largest(player_areas) : owner == 2
                ? Largest(rival_areas) : 0;

            return score;
        }

        /// <summary>
        /// Calculates and Returns Current Score  Difference (for Given player)
        /// </summary>
        /// 
        /// <param name="owner">Player's Number</param>
        /// 
        /// <returns>Calculated Score Difference</returns>
        public int CalculateAdvantage(int owner) => owner == 1 ? CalculateScore(1) - CalculateScore(2) :
                                                        owner == 2 ? CalculateScore(2) - CalculateScore(1) : 0;

        /// <summary>
        /// Calculate Total Score of a Player Due to Regions Only
        /// </summary>
        /// 
        /// <param name="player">Player Value</param>
        /// 
        /// <returns>Regional Score of Player</returns>
        private int GetRegionScore(int player)
        {
            int score = 0;

            foreach (Region region in regions)
                score += region.GetOwner() == player
                    ? region.GetSize() : 0;

            return score;
        }

        /// <summary>
        /// Fetches the Size of the Larges Area of the List
        /// </summary>
        /// 
        /// <param name="area_list">List of the Areas to Compare</param>
        /// 
        /// <returns>Size of the Largest Area of the Given List</returns>
        private int Largest(List<Area> area_list)
        {
            int result = 0;

            for (int slot = 0; slot < area_list.Count; slot++)
                if (area_list[slot].GetSize() > result)
                    result = area_list[slot].GetSize();

            return result;
        }

        /// <summary>
        /// Simpler Access to AreaupdateAlly and AreaUpdateEnemy Methods
        /// </summary>
        /// 
        /// <param name="pos_x">Absciss</param>
        /// 
        /// <param name="pos_y">Ordinate</param>
        /// 
        /// <param name="new_area">Newly Created Area</param>
        /// 
        /// <param name="player">Player Owning the Area</param>
        public void AreaUpdate(int pos_x, int pos_y, ref Area new_area, int player)
        {
            if (player == 1)
                AreaUpdateAlly(pos_x, pos_y, ref new_area);
            if (player == 2)
                AreaUpdateEnemy(pos_x, pos_y, ref new_area);
        }

        /// <summary>
        /// Checks if There's Any Close Area and Merge Them Together
        /// Allied Areas List Version
        /// </summary>
        /// 
        /// <param name="pos_x">Absciss Position of the New Seed</param>
        /// 
        /// <param name="pos_y">Ordinate Position of the New Seed</param>
        /// 
        /// <param name="new_area">Newly Created Area in Stead of the New Seed</param>
        private void AreaUpdateAlly(int pos_x, int pos_y, ref Area new_area)
        {
            player_areas.Add(new_area);

            Area area;
            int slot = 0;
            while (slot < player_areas.Count)
            {
                area = player_areas[slot];
                if (area != new_area && area.AreaIsAdjacentTo(pos_x, pos_y))
                {
                    new_area.Absorb(ref area);
                    player_areas.Remove(area);
                }
                else
                    slot++;
            }
        }

        /// <summary>
        /// Checks if There's Any Close Area and Merge Them Together
        /// Enemy Areas List Version
        /// </summary>
        /// 
        /// <param name="pos_x">Absciss Position of the New Seed</param>
        /// 
        /// <param name="pos_y">Ordinate Position of the New Seed</param>
        /// 
        /// <param name="new_area">Newly Created Area in Stead of the New Seed</param>
        private void AreaUpdateEnemy(int pos_x, int pos_y, ref Area new_area)
        {
            rival_areas.Add(new_area);

            Area area;
            int slot = 0;
            while (slot < rival_areas.Count)
            {
                area = rival_areas[slot];
                if (area != new_area && area.AreaIsAdjacentTo(pos_x, pos_y))
                {
                    new_area.Absorb(ref area);
                    rival_areas.Remove(area);
                }
                else
                    slot++;
            }
        }

        #endregion

        #endregion
    }
}
